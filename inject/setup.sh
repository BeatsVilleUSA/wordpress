#!/bin/bash -x

source /host/settings.sh

### fix permissions
chown www-data: -R /var/www/

### make sure that 'wp' runs as user www-data
cat <<EOF >> /root/.bashrc

### make sure that 'wp' runs as user www-data
alias wp='sudo -u www-data wp'
EOF

### enable mod-ssl and mod-rewrite for apache2
a2enmod ssl rewrite
systemctl restart apache2

### fix the php config
sed -i /etc/php/*/apache2/php.ini \
    -e '/memory_limit/ c memory_limit = 128M' \
    -e '/post_max_size/ c post_max_size = 64M' \
    -e '/max_input_vars/ c max_input_vars = 3000' \
    -e '/max_input_time/ c max_input_time = 60' \
    -e '/upload_max_filesize/ c upload_max_filesize = 64M' \
    -e '/max_execution_time/ c max_execution_time = 180' \
    -e '/default_socket_timeout/ c default_socket_timeout = 180'

systemctl restart apache2
