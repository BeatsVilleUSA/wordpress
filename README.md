# WordPress Container

## Installation

- First install `ds`, `revproxy` and `mariadb`:
  + https://gitlab.com/docker-scripts/ds#installation
  + https://gitlab.com/docker-scripts/revproxy#installation
  + https://gitlab.com/docker-scripts/mariadb#installation

- Then get the scripts: `ds pull wordpress`

- Create a directory for the container: `ds init wordpress @wordpress`

- Fix the settings: `cd /var/ds/wordpress/ ; vim settings.sh`

- Make the container: `ds make`

## Manage a website

- Init a new website: `ds site init site1.example.org`

- Fix the settings: `vim site1.example.org/settings.sh`

- Install wordpress on it: `ds site install site1.example.org`

- Open it in browser and login as admin: https://site1.example.org

- Backup: `ds backup site1.example.org`

- Restore:

  ```
  ds restore site1.example.org \
       backup/wordpress-site1.example.org-2020-07-09.tgz
  ```

- Delete: `ds site del site1.example.org`

# Run wp commands

```
ds wp site1.example.org [...]
```

## Other commands

```
ds shell
ds stop
ds start
ds help
```
