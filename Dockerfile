include(jammy)

### install other tools
RUN apt install --yes sudo git wget curl unzip

### install lamp
RUN DEBIAN_FRONTEND=noninteractive \
    apt install --yes \
        apache2 php libapache2-mod-php php-mysql mysql-client
RUN apt install --yes \
        php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip \
        php-json php-dba php-dev php-pear php-cli php-apcu php-gmp

### install wp-cli
RUN wget https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar && \
    chmod +x wp-cli.phar && \
    mv wp-cli.phar /usr/local/bin/wp
