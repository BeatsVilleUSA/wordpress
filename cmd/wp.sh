cmd_wp_help() {
    cat <<_EOF
    wp <site> [...]
        Manage WordPress through the command-line.
_EOF
}
cmd_wp() {
    local site=$1; shift
    [[ -d $site ]] || fail "Cannot find site '$site'\nUsage:\n$(cmd_wp_help)"
    docker exec -it --user www-data $CONTAINER env TERM=xterm wp --path=/host/$site "$@"
}
