cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    ds restart
    ds inject ubuntu-fixes.sh
    ds inject ssmtp.sh
    ds inject logwatch.sh

    ds inject setup.sh
    update_network_aliases
}
