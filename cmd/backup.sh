cmd_backup_help() {
    cat <<_EOF
    backup <domain>
        Backup the database and the files of the given site.

_EOF
}

cmd_backup() {
    local domain=$1
    [[ -z $domain ]] && fail "Usage:\n$(cmd_backup_help)\n"

    # create a directory for the backup data
    local datestamp=$(date +%F)
    local dir=$CONTAINER-$domain-$datestamp
    rm -rf $dir/
    mkdir -p $dir/

    # export the database
    ds wp $domain db export - > $dir/$domain.sql

    # get the code and config
    cp -a $domain $dir/

    # make the backup archive
    tar --create --gzip --file=$dir.tgz $dir/
    mkdir -p backup
    mv $dir.tgz backup/

    # clean up
    rm -rf $dir/
}
